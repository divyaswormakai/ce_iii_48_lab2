import math

def InsertionSort(arr):
	length = len(arr)
	for i in range(1,length):
		key= arr[i]
		j=i-1
		while(j>=0 and key<arr[j]):
			arr[j+1] = arr[j]
			j-=1
		arr[j+1] =key

def MergeSort(a):
	L1=[]
	R1=[]
	n=len(a)
	if(n<=1):
		return a
	else:
		mid = n//2

		for i in range(0,mid):
			L1.append(a[i])

		for i in range(mid,n):
			R1.append(a[i])
		L2 = MergeSort(L1)
		R2 = MergeSort(R1)

	# L = MergeSort(a[:n//2])
	# R = MergeSort(a[n//2:])

		return merge(L2,R2)

def merge(L,R):
	i=0
	j=i
	n1 = len(L)
	n2 = len(R)
	a=[]

	while(i<n1 and j<n2):
		if(L[i]<= R[j]):
			a.append(L[i])
			i+=1
		else:
			a.append(R[j])
			j+=1
	# If any data left on the array L and R
	while(i<n1):
		a.append(L[i])		
		i+=1
	while(j<n2):
		a.append(R[j])
		j+=1

	return a
