import unittest
from Algorithm import InsertionSort
from Algorithm import MergeSort

class SortTestCase(unittest.TestCase):
	def test_insertion_sort(self):
		arr=[4,2,7,3,6,9,0,1,8]
		sortedArr = [0,1,2,3,4,6,7,8,9]
		InsertionSort(arr)
		self.assertListEqual(arr,sortedArr)

	def test_merge_sort(self):
		arr=[4,2,7,3,6,9,0,1,8]
		sortedArr = [0,1,2,3,4,6,7,8,9]
		InsertionSort(arr)
		self.assertListEqual(arr,sortedArr)


if __name__ == '__main__':
	unittest.main()